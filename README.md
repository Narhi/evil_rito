 

     ________  __     __  ______  __              _______   ______  ________   ______    
    |        \|  \   |  \|      \|  \            |       \ |      \|        \ /      \   
    | $$$$$$$$| $$   | $$ \$$$$$$| $$            | $$$$$$$\ \$$$$$$ \$$$$$$$$|  $$$$$$\  
    | $$__    | $$   | $$  | $$  | $$            | $$__| $$  | $$     | $$   | $$  | $$  
    | $$  \    \$$\ /  $$  | $$  | $$            | $$    $$  | $$     | $$   | $$  | $$  
    | $$$$$     \$$\  $$   | $$  | $$            | $$$$$$$\  | $$     | $$   | $$  | $$  
    | $$_____    \$$ $$   _| $$_ | $$_____       | $$  | $$ _| $$_    | $$   | $$__/ $$  
    | $$     \    \$$$   |   $$ \| $$     \      | $$  | $$|   $$ \   | $$    \$$    $$  
     \$$$$$$$$     \$     \$$$$$$ \$$$$$$$$       \$$   \$$ \$$$$$$    \$$     \$$$$$$  
              
                                                                                 

## Purpose
Project is used to demonstrate how online game rating systems work and how rating or "ELO" adjust to correlate with player skill in the long run.  

## Installation  
npm install  
npm start  
Go to localhost:3000 to see the webpage.  

## Important bits
You can adjust or edit the charts from public/index.html  

You can adjust or edit the code from src/app.ts  

Double-clicking player rating history from the webpage enables/disables other player rating history lines.  

Matchmaking is completely random creating a worst-case scenario from a player rating viewpoint.  

Still, the true skill and elo correlate well. Is it by coincidence?  



