"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const path_1 = __importDefault(require("path"));
// https://github.com/philihp/openskill.js - Simply Genius multiplayer rating system
const openskill_1 = require("openskill");
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
// Player class
class Player {
    // Constructor for the Player class
    constructor(rating, trueSkill) {
        this.rating = rating;
        this.trueSkill = trueSkill;
        this.gamesPlayed = 0;
        this.wins = 0;
        this.ratingHistory = [];
    }
    // Function to get the win rate of a player
    get winRate() {
        return this.gamesPlayed === 0 ? 0 : this.wins / this.gamesPlayed;
    }
    // Get the player's rating (ordinal value)
    getRatingOrdinal() {
        return (0, openskill_1.ordinal)(this.rating);
    }
    // Function to update the rating of a player after a game
    updateRating(newRating, won) {
        this.rating = newRating;
        this.gamesPlayed += 1;
        if (won) {
            this.wins += 1;
        }
        // Update rating history
        this.ratingHistory.push((0, openskill_1.ordinal)(newRating));
    }
}
// Game simulator class
class GameSimulator {
    // Updates player ratings and calculate new elo.
    updateRatings(team1, team2, team1Result, rateFunction) {
        // Elo calculation done here using OpenSkill. Winning team is inserted first, losing second
        const [result1, result2] = rateFunction([team1.map(player => player.rating), team2.map(player => player.rating)]);
        // Updates winning team results
        team1.forEach((player, index) => {
            player.updateRating(result1[index], team1Result);
        });
        // Updates losing team results
        team2.forEach((player, index) => {
            player.updateRating(result2[index], !team1Result);
        });
    }
    // Calculate the total skill of a team
    calculateTeamSkill(team) {
        return team.reduce((sum, player) => sum + player.trueSkill, 0);
    }
    // Apply a logistic function to convert a skill difference into a probability
    calculateWinProbability(skillDifference) {
        return 1.0 / (1.0 + Math.exp(-skillDifference));
    }
    // Determine if an upset has happened
    determineUpset(upsetFactor) {
        return Math.random() < upsetFactor;
    }
    // Determine if Team A has won based on the calculated win probability and upset factor
    determineTeamAWon(probabilityA, hasUpsetHappened) {
        // If upset happens, the team with lower probability wins
        return hasUpsetHappened ? Math.random() > probabilityA : Math.random() < probabilityA;
    }
    // Function to simulate a game between two teams
    simulateGame(teamA, teamB) {
        // Calculate the total skill for each team
        const totalTrueSkillA = this.calculateTeamSkill(teamA);
        const totalTrueSkillB = this.calculateTeamSkill(teamB);
        // Calculate the skill difference between the two teams
        const skillDifference = totalTrueSkillA - totalTrueSkillB;
        // Use a logistic function to calculate the probability of Team A winning
        const probabilityA = this.calculateWinProbability(skillDifference);
        // Determine if an upset has happened (5% chance)
        const upsetFactor = 0.05;
        const hasUpsetHappened = this.determineUpset(upsetFactor);
        // Determine the winning team using the calculated win probability and upset factor
        const teamAWon = this.determineTeamAWon(probabilityA, hasUpsetHappened);
        if (teamAWon) {
            this.updateRatings(teamA, teamB, true, openskill_1.rate);
        }
        else {
            this.updateRatings(teamB, teamA, false, openskill_1.rate);
        }
    }
}
// Adjust these for fun ;)
const NUM_PLAYERS = 1000; // Total number of players
const TEAM_SIZE = 5; // Size of each team
const NUM_SIMULATIONS = 600; // Total number of simulations to run
// Function to generate normally distributed random numbers
function generateNormal(mean, stdDev) {
    const u1 = Math.random();
    const u2 = Math.random();
    const randStdNormal = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2.0 * Math.PI * u2);
    return mean + stdDev * randStdNormal;
}
// Fisher-Yates shuffle algorithm 
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
// Create the players
const players = [];
for (let i = 0; i < NUM_PLAYERS; i++) {
    players.push(new Player((0, openskill_1.rating)(), generateNormal(1000, 500)));
}
// Create the game simulator
const gameSimulator = new GameSimulator();
// Run the simulations
for (let i = 0; i < NUM_SIMULATIONS; i++) {
    // EVIL RITO GIVES ME BAD TEAMS AND NOBODY IS EVEN AT SAME skill level!!! 
    shuffleArray(players);
    // For each set of 10 players, simulate a game
    for (let j = 0; j < NUM_PLAYERS; j += TEAM_SIZE * 2) {
        const teamA = players.slice(j, j + TEAM_SIZE);
        const teamB = players.slice(j + TEAM_SIZE, j + TEAM_SIZE * 2);
        gameSimulator.simulateGame(teamA, teamB);
    }
}
// Express route to get the player data
app.get('/data', (req, res) => {
    console.log(__dirname);
    res.json(players.map(player => ({
        rating: player.getRatingOrdinal(),
        trueSkill: player.trueSkill,
        winRate: player.winRate,
        gamesPlayed: player.gamesPlayed,
        ratingHistory: player.ratingHistory
    })));
});
app.use(express_1.default.static(path_1.default.join(__dirname, 'public')));
// Start the server
const server = app.listen(3000, () => {
    console.log('Server listening on port 3000');
});
server.timeout = 300000; // 5 minutes
//# sourceMappingURL=app.js.map